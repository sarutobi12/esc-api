﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class _version103 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "Supplier",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Supplier", x => x.ID);
            //    });
            //migrationBuilder.AddColumn<int>(
            //    name: "SupplierID",
            //    table: "Ingredients",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //   name: "ModalNameID",
            //   table: "Glues",
            //   nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Supplier");

            //migrationBuilder.DropColumn(
            //    name: "SupplierID",
            //    table: "Ingredients");
            //migrationBuilder.DropColumn(
            //    name: "ModalNameID",
            //    table: "Glues");
        }
    }
}
