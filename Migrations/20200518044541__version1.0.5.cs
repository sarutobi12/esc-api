﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class _version105 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SupplierID",
                table: "Ingredients",
                nullable: true);

            migrationBuilder.AddColumn<int>(
               name: "ModalNameID",
               table: "Glues",
               nullable: true);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Supplier_ingredients_SupplierID",
            //    table: "Ingredients",
            //    column: "SupplierID",
            //    principalTable: "Supplier",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SupplierID",
                table: "Ingredients");
            migrationBuilder.DropColumn(
                name: "ModalNameID",
                table: "Glues");
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Supplier_ingredients_SupplierID",
            //    table: "Ingredients");
        }
    }
}
