﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class _version104 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SupplierID",
                table: "Ingredients",
                nullable: true);

            migrationBuilder.AddColumn<int>(
               name: "ModalNameID",
               table: "Glues",
               nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SupplierID",
                table: "Ingredients");
            migrationBuilder.DropColumn(
                name: "ModalNameID",
                table: "Glues");
        }
    }
}
