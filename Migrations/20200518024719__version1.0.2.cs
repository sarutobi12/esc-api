﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class _version102 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "Supplier",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true),
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Supplier", x => x.ID);
            //    });
            migrationBuilder.DropForeignKey(
                name: "FK_ModelNos_ModelNames_ModelNameID",
                table: "ModelNos");

            migrationBuilder.AlterColumn<int>(
                name: "ModelNameID",
                table: "ModelNos",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "SupplierID",
            //    table: "Ingredients",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //   name: "ModalNameID",
            //   table: "Glues",
            //   nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ModelNos_ModelNames_ModelNameID",
                table: "ModelNos",
                column: "ModelNameID",
                principalTable: "ModelNames",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Supplier");
            migrationBuilder.DropForeignKey(
                name: "FK_ModelNos_ModelNames_ModelNameID",
                table: "ModelNos");

            migrationBuilder.AlterColumn<int>(
                name: "ModelNameID",
                table: "ModelNos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            //migrationBuilder.DropColumn(
            //    name: "SupplierID",
            //    table: "Ingredients");
            //migrationBuilder.DropColumn(
            //    name: "ModalNameID",
            //    table: "Glues");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelNos_ModelNames_ModelNameID",
                table: "ModelNos",
                column: "ModelNameID",
                principalTable: "ModelNames",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
